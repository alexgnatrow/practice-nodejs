'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Locations', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING,
        unique: true,
          validate:{
            notEmpty: true,
            isAlphanumeric: true,
        }
      },
      type: {
        type: Sequelize.STRING,
          validate:{
              isAlphanumeric: true,
          }
      },
      lat: {
        type: Sequelize.INTEGER,
          validate:{
              allowNull: true,
              defaultValue: null,
              validate: { min: -90, max: 90 }
          }
      },
      lng: {
        type: Sequelize.INTEGER,
          validate:{
              allowNull: true,
              defaultValue: null,
              validate: { min: -180, max: 180 }
          }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Locations');
  }
};